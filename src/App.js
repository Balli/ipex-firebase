import React from 'react';
import { Root, Routes, addPrefetchExcludes } from 'react-static';

/** Custom imports */
import { Link, Router } from 'components/Router';
import Chatroom from 'containers/Chatroom';

/** Style Imports */
import GlobalStyle from './globalStyles';

// Any routes that start with 'dynamic' will be treated as non-static routes
addPrefetchExcludes([ 'dynamic' ]);

function App() {
  return (
    <Root>
      <GlobalStyle />
      <nav>
        <Link to="/">Home</Link>
        <Link to="/dynamic-chatroom">Chatroom</Link>
      </nav>
      <div className="content">
        <React.Suspense fallback={<em>Loading...</em>}>
          <Router>
            <Chatroom path="dynamic-chatroom" />
            <Routes path="*" />
          </Router>
        </React.Suspense>
      </div>
    </Root>
  );
}

export default App;
