import React, { Component } from 'react';
import firebase from 'firebase';
import find from 'lodash/find';
import styled from 'styled-components';

import Form from './Form';
import firebaseConfig from '../../firebase.config';

firebase.initializeApp(firebaseConfig);

const SelectStyled = styled.select`
  font-size: 16px;
  background-color: white;
  padding: 6px;
`;

const Wrapper = styled.div`
  margin-left: auto;
  margin-right: auto;
  width: 50%;
  padding-top: 80px;
`;

const users = [
  {
    email: 'user1@example.com',
    password: 'user11',
  },
  {
    email: 'user2@example.com',
    password: 'user12',
  },
  {
    email: 'user3@example.com',
    password: 'user13',
  },
];

class Chatroom extends Component {
  state = {
    user: null,
    email: 'user1@example.com',
    password: 'user11',
  };

  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      this.setState({ user });
    });
  }

  handleSignIn = () => {
    const { email, password } = this.state;

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .catch(function(error) {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.error(
          'Error occured: ',
          errorMessage,
          'with error code: ',
          errorCode,
        );
      });
  };

  handleLogOut = () => {
    firebase.auth().signOut();
  };

  handleUserChange = (e) => {
    const user = find(users, { email: e.target.value });

    this.setState({
      email: e.target.value,
      password: user.password,
    });
  };

  getUsers = () => {
    return (
      <SelectStyled onChange={this.handleUserChange}>
        {users.map((user, index) => (
          <option key={index} value={user.email}>
            {user.email}
          </option>
        ))}
      </SelectStyled>
    );
  };

  render() {
    return (
      <Wrapper>
        <div>
          <h2>Ipex Chatroom</h2>
          {!this.state.user ? (
            <div>
              {this.getUsers()}
              <button onClick={this.handleSignIn}>Login</button>
            </div>
          ) : (
            <button onClick={this.handleLogOut}>Logout</button>
          )}
        </div>
        <div>
          <Form user={this.state.user} />
        </div>
      </Wrapper>
    );
  }
}

export default Chatroom;
