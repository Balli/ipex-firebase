import React, { Component } from 'react';
import Message from 'components/Message';
import firebase from 'firebase';
import styled from 'styled-components';

const StyledInput = styled.input`
  line-height: 32px;
  font-size: 16px;
  padding-left: 6px;
`;

class Form extends Component {
  state = {
    userName: 'Anonymous',
    message: '',
    list: [],
  };

  componentDidMount() {
    this.messageRef = firebase.database().ref().child('messages');
    this.listenMessages();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user) {
      this.setState({ userName: nextProps.user.email });
    }
  }

  handleChange(event) {
    this.setState({ message: event.target.value });
  }

  handleSend() {
    if (this.state.message) {
      var newItem = {
        userName: this.state.userName,
        message: this.state.message,
      };
      this.messageRef.push(newItem);
      this.setState({ message: '' });
    }
  }

  handleKeyPress(event) {
    if (event.key !== 'Enter') return;
    this.handleSend();
  }

  listenMessages() {
    this.messageRef.limitToLast(50).on('value', (message) => {
      this.setState({
        list: Object.values(message.val()),
      });
    });
  }

  getMessages = () => {
    return this.state.list.map(
      (item, index) =>
        index % 2 === 0 ? (
          <Message key={index} message={item} sudo={false} />
        ) : (
          <Message key={index} message={item} sudo />
        ),
    );
  };

  render() {
    return (
      <div>
        <div>{this.getMessages()}</div>
        <div>
          <StyledInput
            type="text"
            placeholder="Type message"
            value={this.state.message}
            onChange={this.handleChange.bind(this)}
            onKeyPress={this.handleKeyPress.bind(this)}
          />
          <button onClick={this.handleSend.bind(this)}>send</button>
        </div>
      </div>
    );
  }
}

export default Form;
