import React, { Component } from 'react';
import styled from 'styled-components';

const MessageWrapper = styled.p`
  background-color: ${(p) => (p.sudo ? '#3f4049' : '#bccc28')};
  color: ${(p) => (p.sudo ? '#bccc28' : '#3f4049')};
  display: block;
  padding: 11px 20px;
  line-height: 1.2;
  font-size: 16px;
  margin-bottom: 0px;
  border-radius: 6px;
  text-align: ${(p) => (p.sudo ? 'right' : 'left')};
`;

class Message extends Component {
  render() {
    return (
      <MessageWrapper sudo={this.props.sudo}>
        <strong>
          {this.props.message.userName}
          {`:  `}
        </strong>
        {this.props.message.message}
      </MessageWrapper>
    );
  }
}

export default Message;
