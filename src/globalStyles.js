import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
* {
  scroll-behavior: smooth;
}

body {
  font-family: 'ConduitITCPro-Medium', sans-serif;
  font-weight: 300;
  font-size: 16px;
  margin: 0;
  padding: 0;
}

a {
  text-decoration: none;
  color: #108db8;
  font-weight: bold;
}

img {
  max-width: 100%;
}

nav {
  width: 100%;
  height: 80px;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1000;
  background-color: #afbf27;
}

nav a {
  color: white;
  padding: 1rem;
  display: inline-block;
  padding-top: 30px;
}

.content {
  padding: 1rem;
}

button {
  display: inline-block;
  background-color: #adbf26;
  margin: 20px;
  padding: 0 16px;
  line-height: 32px;
  font-size: 20px;
  border-radius: 6px;
  color: #3f4049;
`;

export default GlobalStyle;
