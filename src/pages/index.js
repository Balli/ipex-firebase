import React from 'react';
import styled from 'styled-components';

export const Wrapper = styled.div`
  text-align: center;
  padding-top: 80px;
`;

const Home = () => (
  <Wrapper>
    <h1>Welcome to Ipex Chat</h1>
  </Wrapper>
);

export default Home;
